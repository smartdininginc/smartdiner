﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class Restaurant
    {
        public List<Food> foods = new List<Food>();
        public string Name;
        public string Location;
        public int OpeningHourStart;
        public int OpeningHourEnd;
        public double Review;
        public string Type;
        public int PhoneNumber { get; set; }


        public Restaurant(string inName, string inLocation, int inPhoneNumber, int inOpeningsHourStart, int inOpeningHourEnd, double inReview, string inType)
        {
            Name = inName;
            Location = inLocation;
            PhoneNumber = inPhoneNumber; 
            OpeningHourStart = inOpeningsHourStart;
            OpeningHourEnd = inOpeningHourEnd;
            Review = inReview;
            Type = inType; 
        }

        public void addFood(Food myFood)
        {
            foods.Add(myFood);
        }

        public override string ToString()
        {
            return Name;
        }

        
    }
    

}
