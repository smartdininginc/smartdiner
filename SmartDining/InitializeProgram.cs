﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class InitializeProgram
    {

        public static void CreateListOfAllergens()
        {
            User.allAllergens.Add(new Allergen("Æg"));
            User.allAllergens.Add(new Allergen("Laktose"));
            User.allAllergens.Add(new Allergen("Gluten"));
            User.allAllergens.Add(new Allergen("Soja"));
            User.allAllergens.Add(new Allergen("Skaldyr"));
            User.allAllergens.Add(new Allergen("Nødder"));
        }

        public static void LoadRestaurantsAndMeals()
        {
            Restaurant wokkaBox = new Restaurant("Wokkabox", "Vestre Stationsvej 42, 5000 Odense C", 65384359, 1530, 2300, 5, "asian");
            Restaurant umashi = new Restaurant("Umashi", "Nørregade 24, 5000 Odense C", 64183725, 1130, 2300, 4, "asian");
            Restaurant barSushi = new Restaurant("Bar Sushi", "Læssøegade 1, 5000 Odense C", 66136300, 1045, 2200, 3, "asian");
            Restaurant chinaWokHouse = new Restaurant("China Wokhouse", "Overgade 25, 5000 Odense C", 64896564, 1630, 2100, 3, "asian");
            Restaurant daIsabella = new Restaurant("Da Isabella", "Vindegade 67, 5000 Odense C", 66125266, 1230, 2100, 5, "italian");
            Restaurant tonys = new Restaurant("Tonys", "Vindegade 12, 5000 Odense c", 65123514, 1700, 2200, 5, "italian");
            Restaurant theItalian = new Restaurant("The Italian", "Nørregade 15, 5000 Odense C", 64457898, 1300, 2100, 1, "italian");
            Restaurant eraOra = new Restaurant("Era Ora", "Toldbodsgade 11, 5000 Odense C", 65184956, 1700, 2200, 1, "italian");
            Restaurant bergama = new Restaurant("Bergama", "Vesterbro 11, 5000 Odense", 65914001, 1200, 2200, 6.5, "turkish");
            Restaurant sultanPalace = new Restaurant("Sultan Palace", "Georgsgade 15, 5000 Odense C", 64152302, 1200, 0300, 5, "turkish");
            Restaurant ankara = new Restaurant("Restaurant Ankara", "Vestergade 23, Odense C", 63152432, 1530, 2300, 4, "turkish");
            Restaurant özKonyaKebab = new Restaurant("Öz Konya Kebab", "Kongensgade 34, 5000 Odense C", 65123545, 1130, 0500, 4, "turkish");
            Restaurant denGrimmeÆlling = new Restaurant("Den Grimme Ælling", "Hans Jensens Stræde 1, 5000 Odense C", 65917030, 1600, 2200, 3, "danish");
            Restaurant brasserieSkovbakken = new Restaurant("Brasserie Skovbakken", "Rugårdsvej 187, 5210 Odense NV", 65875268, 1730, 2230, 3, "danish");
            Restaurant kokOgVin = new Restaurant("Kok & Vin", "Ternevej 2, 5210 Odense NV", 66748967, 1430, 2300, 5, "danish");
            Restaurant olufBagersGaard = new Restaurant("Restaurant Oluf Bagers Gaard", "Thomas B. Thriges Gade 14, 5000 Odense C", 65891572, 1000, 2200, 2, "danish");
            Restaurant esPåSkovriderkroen = new Restaurant("Restaurant ES på Skovriderkroen", "Duppegade 22, 5000 Odense C", 60131213, 1030, 2000, 4, "danish");
            Restaurant elTorito = new Restaurant("El Torito", "Vestergade 21, 5000 Odense C", 66130202, 1600, 2200, 4, "spanish");
            Restaurant mesonEspana = new Restaurant("Mesón España", "Falen 45, 5000 Odense C", 65847899, 1330, 2200, 5, "spanish");
            Restaurant casetasEspana = new Restaurant("Casetas España", "Vesterbro 77, 5000 Odense C", 88779955, 1630, 2400, 1, "spanish");

            Program.restaurants.Add(wokkaBox);
            Program.restaurants.Add(umashi);
            Program.restaurants.Add(barSushi);
            Program.restaurants.Add(chinaWokHouse);
            Program.restaurants.Add(daIsabella);
            Program.restaurants.Add(tonys);
            Program.restaurants.Add(theItalian);
            Program.restaurants.Add(eraOra);
            Program.restaurants.Add(bergama);
            Program.restaurants.Add(sultanPalace);
            Program.restaurants.Add(ankara);
            Program.restaurants.Add(özKonyaKebab);
            Program.restaurants.Add(denGrimmeÆlling);
            Program.restaurants.Add(brasserieSkovbakken);
            Program.restaurants.Add(kokOgVin);
            Program.restaurants.Add(olufBagersGaard);
            Program.restaurants.Add(esPåSkovriderkroen);
            Program.restaurants.Add(elTorito);
            Program.restaurants.Add(mesonEspana);
            Program.restaurants.Add(casetasEspana);

            //Asiatiske retter
            Food newFood = new Food("Sushi solo", 85.00);
            barSushi.addFood(newFood);
            newFood = new Food("Sushi Duo", 155.00);
            barSushi.addFood(newFood);
            newFood = new Food("Sushi Big", 235.00);
            barSushi.addFood(newFood);
            newFood = new Food("Wokret", 99.00);
            barSushi.addFood(newFood);
            string myString = "Soja";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);

            newFood = new Food("Ret 1", 85.00);
            wokkaBox.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            wokkaBox.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            wokkaBox.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            umashi.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            umashi.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            umashi.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            chinaWokHouse.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            chinaWokHouse.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            chinaWokHouse.addFood(newFood);


            // Italienske retter
            newFood = new Food("Spaghetti Carbonara", 75.00); // Retten
            daIsabella.addFood(newFood);                      // Restauranten som retten tilføjes til
            myString = "Gluten";                              // Allergener i retten
            newFood.addAllergen(myString);
            myString = "Æg";
            newFood.addAllergen(myString);
            myString = "Laktose";
            newFood.addAllergen(myString);
            newFood = new Food("Rejecocktail", 59.00);        // Ny ret
            daIsabella.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Skaldyr";
            newFood.addAllergen(myString);
            newFood = new Food("Tiramisu", 50.00);
            daIsabella.addFood(newFood);
            myString = "Gluten";
            newFood.addAllergen(myString);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Æg";
            newFood.addAllergen(myString);
            newFood = new Food("Bruschetta", 50.00);
            daIsabella.addFood(newFood);
            myString = "Nødder";
            newFood.addAllergen(myString);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Pizza Margherita", 69.00);
            tonys.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Pizza Salami", 79.00);
            tonys.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Pizza Hawaii", 79.00);
            tonys.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Entrecôte alla Grillia", 179.00);
            eraOra.addFood(newFood);
            newFood = new Food("Scallopine al Tartufo", 169.00);
            eraOra.addFood(newFood);
            newFood = new Food("Salmone alla Grillia", 155.00);
            eraOra.addFood(newFood);
            newFood = new Food("Ravioli", 79.00);
            theItalian.addFood(newFood);
            newFood = new Food("Grillet oksemørbrad", 149.00);
            theItalian.addFood(newFood);
            newFood = new Food("Lasagne", 89.00);
            theItalian.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);

            //Tyrkiske retter
            newFood = new Food("Cacik m. brød", 25.00);
            bergama.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Fyldte vinblade", 65.00);
            bergama.addFood(newFood);
            myString = "Æg";
            newFood.addAllergen(myString);
            newFood = new Food("Lammekød m .ris", 75.00);
            bergama.addFood(newFood);
            newFood = new Food("Oksekød m .ris", 75.00);
            bergama.addFood(newFood);
            newFood = new Food("Pandekage m. is", 55.00);
            bergama.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);

            newFood = new Food("Ret 1", 85.00);
            sultanPalace.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            sultanPalace.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            sultanPalace.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            ankara.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            ankara.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            ankara.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            özKonyaKebab.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            özKonyaKebab.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            özKonyaKebab.addFood(newFood);

            // Danske retter         
            newFood = new Food("Tarteletter", 65.00);
            denGrimmeÆlling.addFood(newFood);
            myString = "Æg";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Stegt Flæsk m. persillesovs", 80.00);
            denGrimmeÆlling.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            newFood = new Food("Stjerneskud ", 89.00);
            denGrimmeÆlling.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Ribbensteg", 79.00);
            denGrimmeÆlling.addFood(newFood);
            newFood = new Food("Dansk hjemmelavet flødeis", 65.00);
            denGrimmeÆlling.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Æg";
            newFood.addAllergen(myString);
            newFood = new Food("Koldskål", 45);
            denGrimmeÆlling.addFood(newFood);
            myString = "Æg";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            myString = "Laktose";
            newFood.addAllergen(myString);


            newFood = new Food("Ret 1", 85.00);
            brasserieSkovbakken.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            brasserieSkovbakken.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            brasserieSkovbakken.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            kokOgVin.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            kokOgVin.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            kokOgVin.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            olufBagersGaard.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            olufBagersGaard.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            olufBagersGaard.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            esPåSkovriderkroen.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            esPåSkovriderkroen.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            esPåSkovriderkroen.addFood(newFood);

            //Spanske retter  
            newFood = new Food("Avocadosuppe m. brød", 55.00);
            elTorito.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Hvidløgsbrød", 20.00);
            elTorito.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Burrito m. ris", 75.00);
            elTorito.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Chili Con Carne m. ris", 70.00);
            elTorito.addFood(newFood);
            newFood = new Food("Bananasplit", 55.00);
            elTorito.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);

            newFood = new Food("Ret 1", 85.00);
            mesonEspana.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            mesonEspana.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            mesonEspana.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            casetasEspana.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            casetasEspana.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
        }
    }
}
