﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class Food
    {        
        public List<string> allergens = new List<string>();
        public string Name;
        public double Price;    

        public Food(string inName, double inPrice)
        {
            Name = inName;
            Price = inPrice; 
        }
        public override string ToString()
        {
            return Name + "\t" + "Dkk " + Price;
        }
        public void addAllergen(string myIngredient)
        {
            allergens.Add(myIngredient);
        }

    }
}
