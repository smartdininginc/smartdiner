﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class User
    {
        public static List<Allergen> allAllergens = new List<Allergen>();

        public string UserName;        
        public string UserPassword;
        public string RealName;
        public int PhoneNumber;
        public string Email;
        public List<Food> usersFoods = new List<Food>();
        public List<Allergen> knownAllergens = new List<Allergen>();

        public User(string inUserName, string inUserPassword, string inRealName, int inPhoneNumber, string inEmail)
        {
            UserName = inUserName;
            UserPassword = inUserPassword;
            RealName = inRealName; 
            PhoneNumber = inPhoneNumber;
            Email = inEmail; 
            
        }
        public static void CreateUser(string chosenRealName, string myUserName)
        {
            string userName = " ";
            string userPassword = " ";
            string checkPassword = "";
            string realName = " ";
            int phoneNumber = ' ';
            string email = " ";

            bool whileloop = true;
            while (whileloop)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Velkommen til SmartDining!\nOpret venligst en bruger herunder");
                Console.WriteLine();
                Console.ResetColor();
                Console.Write("Indtast et brugernavn:");
                userName = Console.ReadLine();
                bool usernameNotInUse = true;
                foreach (User user in Program.users)
                {
                    if (user.UserName == userName)
                    {
                        Console.WriteLine("Dette brugernavn eksisterer allerede, vælg venligst et andet brugernavn");
                        System.Threading.Thread.Sleep(2000);
                        usernameNotInUse = false;
                    }
                }

                if (usernameNotInUse)
                {
                    Console.Write("Indtast et password: ");
                    userPassword = Console.ReadLine();
                    Console.Write("Bekræft password: ");
                    checkPassword = Console.ReadLine();
                    if (userPassword == checkPassword)
                    {
                        try
                        {

                            Console.Write("Indtast fulde navn: ");
                            realName = Console.ReadLine();
                            Console.Write("Indtast et telefon nummer: ");
                            phoneNumber = int.Parse(Console.ReadLine());
                            Console.Write("Indtast en gyldig email adresse: ");
                            email = Console.ReadLine();
                            User newUser = new User(userName, userPassword, realName, phoneNumber, email);
                            chosenRealName = realName;
                            Program.myUserName = userName;
                            //userName = myUserName;
                            Program.users.Add(newUser);
                            whileloop = false;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                    }
                    else
                    {
                        Console.WriteLine("Dette password stemmer ikke overens med det valgte password, start oprettelse igen");
                        System.Threading.Thread.Sleep(2000);
                    }
                }
            }

            
        }
        public static void UserInfo(string inMyUserName)
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                Console.WriteLine("Dine informationer");
                Console.WriteLine();
                foreach (User user in Program.users)
                {
                    if (user.UserName == inMyUserName)
                    {
                        Console.WriteLine(user);
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Dine allergener:");
                foreach (User user in Program.users)
                {
                    if (user.UserName == inMyUserName)
                    {
                        for (int i = 0; i < user.knownAllergens.Count; i++)
                        {
                            Console.WriteLine(user.knownAllergens[i]);
                        }
                    }
                }
                
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("0. Gå tilbage til forrige menu");
                Console.WriteLine("1. Rediger brugerdata");
                Console.WriteLine("2. Tilføj allergen");
                int menuValg;

                try
                {
                    menuValg = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    menuValg = 10;
                    System.Threading.Thread.Sleep(3000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    menuValg = 10;
                    System.Threading.Thread.Sleep(3000);
                }

                switch (menuValg)
                {
                    case 0:
                        keepRunning = false;
                        break;
                    case 1:
                        //EditUserInfo();
                        break;
                    case 2:
                        AddKnownAllergen();
                        break;
                    case 10:
                        break;
                    default:
                        Console.WriteLine("Indtast venligst et gyldigt tal");
                        System.Threading.Thread.Sleep(3000);
                        break;
                }
            }
        }

        public void AddToOrder(Food usersFoodItem)
        {
            usersFoods.Add(usersFoodItem);
        }

        private static void AddKnownAllergen()
        {
            bool loopActive = true;
            while (loopActive)
            {
                Console.Clear();
                Console.WriteLine("De mulige allergener til tilføjelse er:");
                int counter = 1;
                foreach (Allergen s in allAllergens)
                {
                    Console.WriteLine(counter + ". " + s.Name);
                    counter++;
                }
                int input = int.Parse(Console.ReadLine());
                switch (input)
                {
                    case 1:
                        AddAllergen("Æg");
                        loopActive = false;
                        break;
                    case 2:
                        AddAllergen("Laktose");
                        loopActive = false;
                        break;
                    case 3:
                        AddAllergen("Gluten");
                        loopActive = false;
                        break;
                    case 4:
                        AddAllergen("Soja");
                        loopActive = false;
                        break;
                    case 5:
                        AddAllergen("Skaldyr");
                        loopActive = false;
                        break;
                    case 6:
                        AddAllergen("Nødder");
                        loopActive = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private static void AddAllergen(string inAllergen)
        {
            Allergen allergen = new Allergen(inAllergen);

            foreach (User user in Program.users)
            {
                if (user.UserName == Program.myUserName)
                {
                    user.knownAllergens.Add(allergen);
                }
            }
        }

        public override string ToString()
        {
            return "Username: \t" + UserName + 
                "\n" + "Password: \t" + UserPassword + 
                "\n" + "Name: \t\t" + RealName + 
                "\n" + "Phone number: \t" + PhoneNumber + 
                "\n" + "Email: \t\t" + Email;
        }
    }
}
