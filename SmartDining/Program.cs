﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class Program
    {
        public static List<Restaurant> restaurants = new List<Restaurant>();
        public static List<User> users = new List<User>();
        public List<Food> allergenFreeFood = new List<Food>();
        public List<Restaurant> chosenRestaurants = new List<Restaurant>();
        string chosenRealName = "";
        int chosenTime = ' ';
        double totalSum = ' ';
        string ChosenWhereToEat = "";
        public static string myUserName = "";
        int personsAtTable = 0;

        static void Main(string[] args)
        {
            Program myProgram = new Program();
            myProgram.Run();
        }

        private void Run()
        {
            InitializeProgram.CreateListOfAllergens();
            InitializeProgram.LoadRestaurantsAndMeals();
            LoadAdminUser();
            SignInMenu();
        }

        private void LoadAdminUser()
        {
            User myUser = new User("Admin", "Admin", "Admin Adminson", 12345678, "Admin@Admin.Admin");
            users.Add(myUser);
        }

        public void SignInMenu()
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                int input = ' ';
                Console.WriteLine("Velkommen til SmartDining");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("0. Afslut program");
                Console.WriteLine("1. Opret en bruger");
                Console.WriteLine("2. Login");

                try
                {
                    input = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    input = 10;
                    System.Threading.Thread.Sleep(3000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    input = 10;
                    System.Threading.Thread.Sleep(3000);
                }

                switch (input)
                {
                    case 0:
                        keepRunning = false;
                        break;
                    case 1:
                        User.CreateUser(chosenRealName, myUserName);
                        OverviewMenu();
                        break;
                    case 2:
                        SignInExistingUser();
                        break;
                    case 10:
                        break;
                    default:
                        Console.WriteLine("Indtast venligst et gyldigt tal");
                        System.Threading.Thread.Sleep(3000);
                        break;
                }
            }
        }

        private void SignInExistingUser()
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                string InputUserName = " ";
                string InputUserPassword = " ";
                bool UserFound = false;
                Console.Clear();
                Console.Write("Brugernavn:");
                InputUserName = Console.ReadLine();
                Console.Write("Password:");
                InputUserPassword = Console.ReadLine();

                foreach (User user in users)
                {
                    if (InputUserName == user.UserName && InputUserPassword == user.UserPassword)
                    {
                        chosenRealName = user.RealName;
                        myUserName = user.UserName;
                        UserFound = true;
                    }
                }

                if (UserFound)
                {
                    keepRunning = false;
                    OverviewMenu();
                }
                else
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Brugeren blev ikke fundet, tjek om brugernavn eller password er forkert");
                    Console.WriteLine("Vent venligst mens vi dirigerer dig tilbage til oprettelse");
                    Console.ResetColor();
                    System.Threading.Thread.Sleep(3500);
                }
            }
        }
        
        private void OverviewMenu()
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                int input = ' ';
                Console.WriteLine("Velkommen til SmartDining " + "\n" + chosenRealName);
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("1. Vælg restaurant ud fra kategori");
                Console.WriteLine("2. Brugerprofil");
                Console.WriteLine("3. Vis liste over restauranter der bruger SmartDining");
                Console.WriteLine("4. Se tilbud");
                Console.WriteLine("5. Anmeld Restaurant");
                Console.WriteLine("6. Se restauranters nyeste reviews");
                Console.WriteLine("7. Se restauranters åbningstider");
                Console.WriteLine("8. Logud");
                try
                {
                    input = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    input = 10;
                    System.Threading.Thread.Sleep(3000);
                    OverviewMenu();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    input = 10;
                    System.Threading.Thread.Sleep(3000);
                    OverviewMenu();
                }

                switch (input)
                {
                    case 1:
                        SelectRestaurantCategory();
                        break;
                    case 2:
                        User.UserInfo(myUserName);
                        break;
                    case 3:
                        AllRestaurants();
                        break;
                    case 4:
                        Offers();
                        break;
                    case 5:
                        GiveReview();
                        break;
                    case 6:
                        PrintReview();
                        break;
                    case 7:
                        PrintOpeningHours();
                        break;
                    case 8:
                        keepRunning = false; // Går tilbage til SignInMenu
                        break;
                    default:
                        Console.WriteLine("Indtast venligst et gyldigt tal");
                        System.Threading.Thread.Sleep(3000);
                        break;
                }
            }
        }

        private void SelectRestaurantCategory()
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                int input = ' ';
                Console.Clear();
                Console.WriteLine("Restauranter: ");
                Console.WriteLine("");
                Console.WriteLine("0. Tilbage");
                Console.WriteLine("1. Italienske restauranter");
                Console.WriteLine("2. Spanske restauranter");
                Console.WriteLine("3. Danske restauranter");
                Console.WriteLine("4. Tyrkiske restauranter");
                Console.WriteLine("5. Asiatiske restauranter");
                try
                {
                    input = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast et tal");
                    input = 10;
                    System.Threading.Thread.Sleep(3000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    input = 10;
                    System.Threading.Thread.Sleep(3000);
                }

                switch (input)
                {
                    case 0:
                        keepRunning = false; // Går tilbage til OverviewMenu
                        break;
                    case 1:
                        ShowRestaurantsFromChosenCategory("italian");
                        break;
                    case 2:
                        ShowRestaurantsFromChosenCategory("spanish");
                        break;
                    case 3:
                        ShowRestaurantsFromChosenCategory("danish");
                        break;
                    case 4:
                        ShowRestaurantsFromChosenCategory("turkish");
                        break;
                    case 5:
                        ShowRestaurantsFromChosenCategory("asian");
                        break;
                    case 10:
                        break;
                    default:
                        Console.WriteLine("Indtast venligst et gyldigt tal");
                        System.Threading.Thread.Sleep(3000);
                        break;
                }
            }
        }

        private void ShowRestaurantsFromChosenCategory(string type)
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                allergenFreeFood.Clear();
                foreach (User user in users)
                {
                    if (user.UserName == myUserName)
                    {
                        user.usersFoods.Clear();
                        totalSum = 0;
                    }
                }
                int count = 0;
                chosenRestaurants.Clear();
                int choice = ' ';
                Console.WriteLine("Vælg Restaurant:\n");
                Console.WriteLine("0. Tilbage");
                foreach (Restaurant restaurant in restaurants)
                    if (restaurant.Type == type)
                    {
                        count++;
                        Console.WriteLine("{0}.{1}", count, restaurant);
                        chosenRestaurants.Add(restaurant);
                    }

                try
                {
                    choice = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                    choice = -1; // Spørger bruger om input igen
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                    choice = -1;
                }

                if (choice == 0)
                    keepRunning = false; // Går tilbage til SelectRestaurantCategory
                else if (choice > 0 && choice <= count)
                    Restaurant(chosenRestaurants[choice - 1]);
                else if (choice < -1 || choice > count)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(3000);
                }
                else if (choice == -1) { }
                    // Spørger bruger om input igen
            }
        }

        private void Restaurant(Restaurant myRestaurant)
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Velkommen til " + myRestaurant);
                Console.WriteLine();
                Console.ResetColor();
                int userChoice = ' ';
                Console.WriteLine("0. Tilbage til liste over restaurantkategorier");
                Console.WriteLine("1. Tilbage til liste over alle restauranter der bruger SmartDining");
                Console.WriteLine("2. Vis menu uden allergener");
                Console.WriteLine("3. Vis menu med alle retter");

                try
                {
                    userChoice = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                    Restaurant(myRestaurant);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                    Restaurant(myRestaurant);
                }

                switch (userChoice)
                {
                    case 0:
                        keepRunning = false; // Går tilbage til SelectRestaurantCategory()
                        break;
                    case 1:
                        AllRestaurants();
                        break;
                    case 2:
                        AllergenFreeMenu(FindUserAllergens(), myRestaurant);
                        break;
                    case 3:
                        NormalMenu(myRestaurant);
                        break;
                    default:
                        Console.WriteLine("Indtast venligst et gyldigt tal");
                        System.Threading.Thread.Sleep(3000);
                        break;
                }
            }
        }

        private List<string> FindUserAllergens()
        {
            List<string> currentUserKnownAllergens = new List<string>();
            foreach (User user in users)
            {
                if (user.UserName == myUserName)
                {
                    foreach (Allergen allergenName in user.knownAllergens)
                    {
                        currentUserKnownAllergens.Add(allergenName.Name);
                    }
                    return currentUserKnownAllergens;
                }
            }
            return null;
        }

        private void AllRestaurants()
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                int userInput = ' ';
                allergenFreeFood.Clear();
                foreach (User user in users)
                {
                    if (user.UserName == myUserName)
                    {
                        user.usersFoods.Clear();
                        totalSum = 0;
                    }
                }
                Console.WriteLine("Restauranter der bruger SmartDining");
                Console.WriteLine();
                Console.WriteLine("0. Tilbage");
                int count = 0;

                foreach (Restaurant restaurant in restaurants)
                {
                    count++;
                    Console.WriteLine("{0}.{1}", count, restaurant);
                }

                try
                {
                    userInput = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                    userInput = -1;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                    userInput = -1;
                }

                if (userInput == 0)
                    keepRunning = false; // Går tilbage til SelectRestaurantCategory
                else if (userInput > 0 && userInput <= count)
                    Restaurant(restaurants[userInput - 1]);
                else if (userInput < -1 || userInput > count)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(3000);
                }
                else if (userInput == -1) { }
                    // Spørger bruger om input igen
            }
        }

        private void AllergenFreeMenu(List<string> inCurrentUserKnownAllergens, Restaurant myRestaurant)
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                allergenFreeFood.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Retter uden allergener\n");
                Console.ResetColor();
                int count = 0;
                int userInput = ' ';
                Console.WriteLine("0. Tilbage\n");
                foreach (Food food in myRestaurant.foods)
                {
                    bool foundAllergen = false;
                    foreach (string s in food.allergens)
                    {
                        if (inCurrentUserKnownAllergens.Contains(s))
                        {
                            foundAllergen = true;
                        }
                    }

                    if (!foundAllergen)
                    {
                        count++;
                        Console.WriteLine("{0}.{1}", count, food);
                        if (!allergenFreeFood.Contains(food))
                        {
                            allergenFreeFood.Add(food);
                        }
                    }
                }

                if (count == 0)
                {
                    Console.WriteLine("\nDer findes ingen retter uden allergener");
                }
                else
                {
                    Console.WriteLine("\n{0}. Vælg et tilfældigt måltid", count + 1);
                }

                try
                {
                    userInput = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                }

                if (userInput == 0)
                    keepRunning = false; // Går tilbage til Restaurant(Restaurant myRestaurant)
                else if (userInput == count + 1)
                    RandomizeAllergenFreeMeal(myRestaurant);
                else if (userInput > 0 && userInput <= count)
                {
                    foreach (User user in users)
                    {
                        if (user.UserName == myUserName)
                        {
                            user.AddToOrder(allergenFreeFood[userInput - 1]);
                            TotalSum(allergenFreeFood[userInput - 1]);
                        }
                    }

                    OrderMoreMeals(myRestaurant, "allergen free menu", inCurrentUserKnownAllergens);
                }
                else if (userInput < -1 || userInput > count)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(3000);
                }
                else if (userInput == -1)
                {
                    // Spørger brugeren om input igen
                }
            }
        }

        private void OrderMoreMeals(Restaurant myRestaurant, string menuType, List<string> inCurrentUserKnownAllergens)
        {
            bool keepRunning = true;
            int userInput = -1;
            while (keepRunning)
            {
                Console.Clear();
                PrintUserOrder();
                Console.Write("Ønsker du at tilføje flere ting til din ordre? \n" +
                    "1. Ja  \n" +
                    "2.nej \n");

                try
                {
                    userInput = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                    userInput = -1;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                    userInput = -1;
                }

                switch (userInput)
                {
                    case -1:
                        // Spørger brugeren om input igen
                        break;
                    case 1:
                        keepRunning = false;
                        if (menuType == "allergen free menu")
                        {
                            AllergenFreeMenu(inCurrentUserKnownAllergens, myRestaurant);
                            break;
                        }
                        else if (menuType == "normal menu")
                        {
                            NormalMenu(myRestaurant);
                            break;
                        }
                        else
                            break;
                    case 2:
                        keepRunning = false;
                        HandleOrderDetails(myRestaurant);
                        break;
                    default:
                        Console.WriteLine("Indtast venligst et gyldigt tal");
                        System.Threading.Thread.Sleep(3000);
                        break;
                }
            }
        }

        public void RandomizeAllergenFreeMeal(Restaurant myRestaurant)
        {
            bool keeprunning = true;
            while (keeprunning)
            {
                Console.Clear();
                int order = ' ';
                int x = allergenFreeFood.Count();
                Random randomizer = new Random((int)DateTime.Now.Millisecond);
                int i = randomizer.Next(0, x);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Randomizeren har valgt: " + allergenFreeFood[i]);
                Console.ResetColor();
                Console.WriteLine(" Vil du tilføje dette til din ordre?\n"+
                    "1. Ja \n"+
                    "2. Nej");

                try
                {
                    order = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                }

                if (order == 1)
                {
                    foreach (User user in users)
                    {
                        if (user.UserName == myUserName)
                        {
                            user.AddToOrder(allergenFreeFood[i]);
                            TotalSum(myRestaurant.foods[i]);
                        }
                    }
                    keeprunning = false;
                }

                else if (order == 2)
                {
                    Console.WriteLine("Vil du 1. prøve at randomize igen? eller 2. returnere til menukortet");
                    int userChoice = int.Parse(Console.ReadLine());
                    if (userChoice == 1)
                    {
                        // kører loop igen
                    }

                    else if (userChoice == 2)
                    {
                        keeprunning = false;
                        Restaurant(myRestaurant);
                    }
                }
            }
        }

        private void NormalMenu(Restaurant myRestaurant)
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Menukort\n");
                Console.ResetColor();
                int count = 0;
                Console.WriteLine("0. Tilbage\n");
                int userInput = ' ';

                foreach (Food food in myRestaurant.foods)
                {
                    count += 1;
                    Console.WriteLine("{0}.{1}", count, food);
                }
                Console.WriteLine("\n{0}. Vælg et tilfældigt måltid", count + 1);

                try
                {
                    userInput = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                    userInput = -1;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                    userInput = -1;
                }

                if (userInput == 0)
                    keepRunning = false; // Går tilbage til Restaurant(myRestaurant)
                else if (userInput == count + 1)
                    RandomizeNormalMeal(myRestaurant);
                else if (userInput > 0 && userInput <= count)
                {
                    foreach (User user in users)
                    {
                        if (user.UserName == myUserName)
                        {
                            user.AddToOrder(myRestaurant.foods[userInput - 1]);
                            TotalSum(myRestaurant.foods[userInput - 1]);
                        }
                    }

                    OrderMoreMeals(myRestaurant, "normal menu", null);
                }
                else if (userInput < -1 || userInput > count)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(3000);
                }
                else if (userInput == -1)
                {
                    // Spørger brugeren om input igen
                }
            }
        }

        private void HandleOrderDetails(Restaurant myRestaurant)
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                foreach (User user in users)
                {
                    if (user.UserName == myUserName)
                    {
                        Console.Clear();
                        int whereToEat = ' ';
                        int finalChoice = ' ';
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Indtast venligst tidspunktet du ønsker din mad klar på (Format 0000)");
                        Console.ResetColor();

                        try
                        {
                            chosenTime = int.Parse(Console.ReadLine());
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Forkert input, indtast venligst et tal");
                            System.Threading.Thread.Sleep(3000);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            System.Threading.Thread.Sleep(3000);
                        }

                        if (chosenTime >= (myRestaurant.OpeningHourStart + 15) && chosenTime < (myRestaurant.OpeningHourEnd))
                        {
                            Console.Clear();
                            Console.WriteLine("Din mad er bestilt til kl. {0}.", chosenTime);
                            System.Threading.Thread.Sleep(3000);
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Vælg venligst om du vil have maden: 1.to stay/2.to go");
                            Console.ResetColor();
                            try
                            {
                                whereToEat = int.Parse(Console.ReadLine());
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Forkert input, indtast venligst et tal");
                                System.Threading.Thread.Sleep(3000);
                                whereToEat = -1;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                System.Threading.Thread.Sleep(3000);
                                whereToEat = -1;
                            }

                            switch (whereToEat)
                            {
                                case -1:
                                    // Spørger bruger om input igen
                                    break;
                                case 1:
                                    Console.Clear();
                                    ChosenWhereToEat = "to stay";
                                    Console.WriteLine("Du har valgt at spise på restauranten KL. " + chosenTime);
                                    Console.WriteLine("Indtast venligst hvor mange personer du vil bestille bord til.");
                                    try
                                    {
                                        personsAtTable = int.Parse(Console.ReadLine());
                                        Console.WriteLine("Du har bestilt bord til: " + personsAtTable);
                                        System.Threading.Thread.Sleep(3000);
                                        WriteOrderForConfirmation(user);
                                    }
                                    catch (FormatException)
                                    {
                                        Console.WriteLine("Forkert input, indtast venligst et tal");
                                        System.Threading.Thread.Sleep(3000);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                        System.Threading.Thread.Sleep(3000);
                                    }
                                    break;
                                case 2:
                                    Console.Clear();
                                    ChosenWhereToEat = "to go";
                                    Console.WriteLine("Din mad er bestilt som takeaway, maden er bestilt til kl " + chosenTime);
                                    System.Threading.Thread.Sleep(3000);
                                    WriteOrderForConfirmation(user);
                                    break;
                                default:
                                    Console.WriteLine("Indtast venligst et gyldigt tal");
                                    System.Threading.Thread.Sleep(3000);
                                    break;
                            }

                            if (personsAtTable > 0 && ChosenWhereToEat == "to stay")
                            {
                                Console.Write("Valgt {0} kl {1} og har bestilt bord til {2}.\n1.Bekræft 2. Afbryd\n", ChosenWhereToEat, chosenTime, personsAtTable);
                            }
                            else if (ChosenWhereToEat == "to go")
                            {
                                Console.Write("Valgt {0} kl {1}.\n1.Bekræft 2. Afbryd\n", ChosenWhereToEat, chosenTime);
                            }
                            else
                            {
                                finalChoice = -1;
                            }

                            if (finalChoice != -1)
                            {
                                try
                                {
                                    finalChoice = int.Parse(Console.ReadLine());
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Forkert input, indtast venligst et tal");
                                    System.Threading.Thread.Sleep(3000);
                                    finalChoice = -1;
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                    System.Threading.Thread.Sleep(3000);
                                    finalChoice = -1;
                                }
                            }

                            switch (finalChoice)
                            {
                                case -1:
                                    // Spørger bruger om input igen
                                    break;
                                case 1:
                                    WriteToFile(user);
                                    Console.Clear();
                                    Console.WriteLine("Tak for din ordre, den er nu sendt til restauranten, afvent bekræftelse fra " + myRestaurant.Name);
                                    Console.WriteLine("Hvis du har problemer med din ordre kontakt venligst " + myRestaurant.Name + " på " + myRestaurant.PhoneNumber);
                                    PaymentAndOrderConfirmation();
                                    keepRunning = false;
                                    break;
                                case 2:
                                    user.usersFoods.Clear();
                                    totalSum = 0;
                                    keepRunning = false;
                                    break;
                                default:
                                    Console.WriteLine("Indtast venligst et gyldigt tal");
                                    System.Threading.Thread.Sleep(3000);
                                    break;
                            }
                        }

                        else
                        {
                            Console.WriteLine("Du har prøvet at bestille maden til udenfor restaurantens åbningstid, bestil venligst inden for tidsrummet {0} til {1}", (myRestaurant.OpeningHourStart + 15), (myRestaurant.OpeningHourEnd));
                            System.Threading.Thread.Sleep(3500);
                        }
                    }
                }
            }
        }

        private void PaymentAndOrderConfirmation()
        {
            Console.WriteLine("Du bliver nu dirigeret videre til betaling");
            System.Threading.Thread.Sleep(6000);
            //Metode der fører til betaling
            Console.WriteLine("Din betaling er godkendt.");
            Console.WriteLine("Tak for din ordre, den er nu sendt til restauranten, Vent venligst på bekræftigelse. Dette kan tage et par minutter...");
            System.Threading.Thread.Sleep(5000);
            Console.Clear();
            Console.WriteLine("Din ordre er nu bekræftet.");
            Console.WriteLine("Tryk på en vilkårlig tast for at begynde på en ny bestilling");
            Console.ReadKey();

            //Besked sendes til restauranten om at gå i gang med ordren
        }

        private void WriteOrderForConfirmation(User user)
        {
            Console.Clear();
            Console.WriteLine("Du har bestilt:");
            foreach (Food food in user.usersFoods)
            {
                Console.WriteLine(food);
            }
        }

        public void RandomizeNormalMeal(Restaurant myRestaurant)
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                int x = myRestaurant.foods.Count();
                Random randomizer = new Random((int)DateTime.Now.Millisecond);
                int i = randomizer.Next(0, x);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Randomizeren har valgt: " + myRestaurant.foods[i]);
                Console.ResetColor();
                Console.WriteLine("Vil du tilføje dette til din ordre?\n"+
                    "1. Ja \n"+
                    "2. Nej");

                int order = ' ';
                int userChoice = ' ';

                try
                {
                    order = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                }

                if (order == 1)
                {
                    foreach (User user in users)
                    {
                        if (user.UserName == myUserName)
                        {
                            user.AddToOrder(myRestaurant.foods[i]);
                            TotalSum(myRestaurant.foods[i]);
                        }
                    }
                    keepRunning = false;
                }

                else if (order == 2)
                {
                    Console.WriteLine("Vil du 1. prøve at randomize igen? eller 2. returnere til menukortet");
                    try
                    {
                        userChoice = int.Parse(Console.ReadLine());
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Forkert input, indtast venligst et tal");
                        System.Threading.Thread.Sleep(3000);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        System.Threading.Thread.Sleep(3000);
                    }

                    if (userChoice == 1)
                    {
                        // Kører loop igen
                    }

                    else if (userChoice == 2)
                    {
                        keepRunning = false;
                        Restaurant(myRestaurant);
                    }
                }
            }
        }
        
        private void Offers()
        {
            Console.Clear();
            Console.WriteLine("Der er ikke nogle tilbud lige nu. ");
            Console.ReadKey();
        }

        private void GiveReview()
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                string userRestaurant = "";
                int userReview = ' ';
                bool restaurantFound = false;
                string chosenRestaurant = "";
                Console.Clear();
                Console.WriteLine("0. Tilbage");
                Console.WriteLine();
                Console.WriteLine("Indtast navn på den restaurant du ønsker at anmelde: ");
                userRestaurant = Console.ReadLine();
                if (userRestaurant == "0")
                {
                    keepRunning = false;
                }
                else
                {
                    foreach (Restaurant restaurant in restaurants)
                    {
                        if (restaurant.Name == userRestaurant)
                        {
                            restaurantFound = true;
                            chosenRestaurant = restaurant.Name;
                        }
                    }
                    if (restaurantFound)
                    {
                        Console.Clear();
                        Console.WriteLine("0. Tilbage");
                        Console.WriteLine("");
                        Console.WriteLine("Indtast venligst antal stjerner du ønsker at give restauranten (1-5 stjerner)");
                        try
                        {
                            userReview = int.Parse(Console.ReadLine());
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Forkert input, indtast venligst et tal");
                            System.Threading.Thread.Sleep(3000);
                            userReview = -1;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            System.Threading.Thread.Sleep(3000);
                            userReview = -1;
                        }

                        switch (userReview)
                        {
                            case -1:
                            case 0:
                                // Spørger bruger om input igen
                                break;
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                foreach (Restaurant restaurant in restaurants)
                                {
                                    if (restaurant.Name == chosenRestaurant)
                                    {
                                        restaurant.Review = userReview;
                                        Console.WriteLine("Du har nu givet {0} {1} stjene(r). Vent venligst mens vi dirigerer dig til menuen.", restaurant, userReview);
                                        System.Threading.Thread.Sleep(3000);
                                        keepRunning = false;
                                    }
                                }
                                break;
                            default:
                                Console.WriteLine("Du skal indtaste et tal mellem 1 og 5");
                                System.Threading.Thread.Sleep(3000);
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Restauranten blev desværre ikke fundet, prøv igen");
                        System.Threading.Thread.Sleep(3000);
                    }
                }
            }
        }

        private void PrintReview()
        {
            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                int userInput = ' ';
                Console.WriteLine("Restauranternes nyeste anmeldelse");
                Console.WriteLine();
                Console.WriteLine("0. Tilbage");
                int count = 1;
                foreach (Restaurant restaurant in restaurants)
                {
                    Console.Write("{0}. {1} - {2}", count, restaurant.Name, restaurant.Review);
                    Console.WriteLine();
                    count++;
                }
                try
                {
                    userInput = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(3000);
                    userInput = -1;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                    userInput = -1;
                }

                if (userInput == -1)
                {
                    // Spørger bruger om input igen
                }
                else if (userInput == 0)
                    keepRunning = false;
                else if (userInput > 0 && userInput <= count)
                {
                    Restaurant(restaurants[userInput - 1]);
                    keepRunning = false;
                }
                else if (userInput < -1 || userInput > count)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(3000);
                }
            }
        }

        private void PrintOpeningHours()
        {
            Console.Clear();
            int userInput = ' ';
            Console.WriteLine("Restauranternes åbningstider");
            Console.WriteLine();
            Console.WriteLine("0. Tilbage");
            int count = 1;
            foreach (Restaurant restaurant in restaurants)
            {
                Console.Write("{0}. {1} - {2} - {3}", count, restaurant.Name, restaurant.OpeningHourStart, restaurant.OpeningHourEnd);
                Console.WriteLine();
                count++;
            }
            try
            {
                userInput = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(3000);
                PrintReview();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                System.Threading.Thread.Sleep(3000);
                PrintReview();
            }

            if (userInput == 0)
            {
                OverviewMenu();
            }
            else
            {
                try
                {
                    Restaurant(restaurants[userInput - 1]);
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(3000);
                    PrintReview();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    System.Threading.Thread.Sleep(3000);
                    PrintReview();
                }
            }
            Console.ReadLine();
        }

        private void SignOut()
        {
            foreach (User user in users)
            {
                chosenRealName = "";
                chosenTime = ' ';
                totalSum = ' ';
                ChosenWhereToEat = "";
                myUserName = "";
                users.Clear();
                SignInMenu();
            }
        }

        private void PrintUserOrder()
        {
            foreach (User user in users)
            {
                if (user.UserName == myUserName)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Din ordre er nu: ");
                    Console.ResetColor();
                    foreach (Food food in user.usersFoods)
                    {
                        Console.WriteLine(food);
                    }
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Total pris af din ordre: DKK " + totalSum);
                    Console.ResetColor();
                }
            }
        }

        public void WriteToFile(User myUser)
        {
            string filepath = Environment.ExpandEnvironmentVariables(@"C:\\Users\\%USERNAME%\\Desktop\\SmartDining");
            string path = Environment.ExpandEnvironmentVariables(@"C:\\Users\\%USERNAME%\\Desktop\\SmartDining\\Ordrer.txt");
            Environment.CurrentDirectory = Environment.ExpandEnvironmentVariables("C:\\Users\\%USERNAME%\\Desktop");
            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }
            if (!File.Exists(path))
            {
                File.Create(@path).Dispose();
            }
            using (StreamWriter file = new StreamWriter(@path, true))
            {
                file.WriteLine("Bestillingstidspunkt: " + DateTime.Now);
                file.WriteLine();
                file.WriteLine("Kundens navn: " + myUser.RealName);
                file.WriteLine();
                file.WriteLine("Bestilling: ");
                foreach (Food myFood in myUser.usersFoods)
                {
                    file.WriteLine(myFood + " ");
                }
                file.WriteLine();
                file.WriteLine("Ordrens total pris: DKK " + totalSum);
                file.WriteLine();
                file.WriteLine("Tid ordren er bestilt til: " + chosenTime);
                file.WriteLine();
                file.WriteLine("Kunden ønsker ordren: " + ChosenWhereToEat);
                file.WriteLine();
                if (ChosenWhereToEat == "to stay")
                {
                    file.WriteLine("Der ønskes bord til {0}", personsAtTable);
                    file.WriteLine();
                }
            }
        }

        public double TotalSum(Food food)
        {
            totalSum += food.Price;
            return totalSum;
        }
    }
}
