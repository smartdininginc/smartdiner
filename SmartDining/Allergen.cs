﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class Allergen
    {
        public string Name;
        
        
        public Allergen(string inName)
        {
            Name = inName; 
        }

        public override string ToString()
        {
            return Name;
        }



    }
}
