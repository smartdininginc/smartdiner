﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class Order
    {
        public string RealName;
        public double Time;
        public List<Food> Food = new List<Food>(); 
        

        public Order(string inRealName, List<Food> inFood, double inTime)
        {
            RealName = inRealName;
            Food = inFood;
            Time = inTime;
        }
    }
}
